function getApiKey() {
    return localStorage.getItem('apiKey');
}

function saveApiKey(apiKey) {
    return localStorage.setItem('apiKey', apiKey);
}

function getToken() {
    return localStorage.getItem('trello_token');
}

function clearCredentials() {
    localStorage.removeItem('trello_token');
    localStorage.removeItem('apiKey');
}

function saveSelectedBoardAndList(boardId, listId, boardName, listName) {
    localStorage.setItem('boardId', boardId);
    localStorage.setItem('listId', listId);
    localStorage.setItem('boardName', boardName);
    localStorage.setItem('listName', listName);
}

function getSelectedBoardName() {
    return localStorage.getItem('boardName');
}

function getSelectedListName() {
    return localStorage.getItem('listName');
}

function getSelectedBoardId() {
    return localStorage.getItem('boardId');
}

function getSelectedListId() {
    return localStorage.getItem('listId');
}

function getJiraAddresses() {
    return JSON.parse(localStorage.getItem('jira_address'));
}

function saveJiraAddresses(address) {
    localStorage.setItem('jira_address', address);
}

function getTaskData(issueId, callback) {
    chrome.storage.sync.get('added_tasks', function(items) {
        let addedTasks = items.added_tasks;
        if (addedTasks) {
            callback(addedTasks[issueId]);
        } else {
            callback(null);
        }
    });
}

function saveTaskData(issueId, data) {
    chrome.storage.sync.get('added_tasks', function(items) {
        if (!items.added_tasks) {
            items.added_tasks = {};
        }
        items.added_tasks[issueId] = data;
        chrome.storage.sync.set({'added_tasks': items.added_tasks});
    });
}
