function getConditionsToShowAction() {
    let ticketPath = 'browse/';
    let conditions = [];
    let jiraAddresses = getJiraAddresses();
    $.each(jiraAddresses, function(i, url) {
        conditions.push(
            new chrome.declarativeContent.PageStateMatcher({
                pageUrl: {urlContains: url + ticketPath},
            })
        );
    });
    return conditions;
}

function updatePageRules() {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
        chrome.declarativeContent.onPageChanged.addRules([
            {
                conditions: getConditionsToShowAction(),
                actions: [new chrome.declarativeContent.ShowPageAction()],
            },
        ]);
    });
}

function authenticationSuccess() {
    $('#msg-successful').show();
}

function authenticationFailure() {
    $('#msg-failed').show();
}

function saveFormData() {
    let jiraAddresses = $('textarea[name=\'field-jira\']').val();
    jiraAddresses = (jiraAddresses) ? jiraAddresses.split('\n') : [];
    $.each(jiraAddresses, function(i, url) {
        url = url.replace(/\/?$/, '/');
        jiraAddresses[i] = url.trim();
    });
    saveJiraAddresses(JSON.stringify(jiraAddresses));
    updatePageRules();
    $('#msg-saved').show().delay(500).fadeOut();
}

function loadFormData() {
    let jiraAddresses = getJiraAddresses();
    jiraAddresses = ((jiraAddresses) ? jiraAddresses.join('\n') : '');
    $('textarea[name=\'field-jira\']').val(jiraAddresses);
}

$(function() {
    if (HashSearch.keyExists('token')) {
        authenticateAccount(
            false, authenticationSuccess, authenticationFailure);
    }
    if (!getToken()) {
        $('#form-settings').hide();
        $('#btn-connect').click(function() {
            getApiCredentials(function(apiKey) {
                saveApiKey(apiKey);
                authenticateAccount(
                    true, authenticationSuccess, authenticationFailure);
            }, function(message) {
                $('#msg-connect-failure').text(message).show();
            });
        });
        $('#btn-connect').show();
    } else {
        $('#form-settings').show();
    }

    $('#form-settings').submit(function(event) {
        event.preventDefault();
        saveFormData();
    });
    loadFormData();
});
