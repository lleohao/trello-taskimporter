function highlightLabel(label) {
    label.animate({color: '#ee6363'}, 200)
        .delay(100).animate({color: '#000'}, 200);
}

function addCardSuccess(jiraIssueId) {
    return function(data) {
        $('#add-card').hide();
        $('#simple-add').hide();
        $('#msg-successful').show('bounce');

        let cardUrl = data.shortUrl;
        $('.card-link').attr('href', cardUrl).text(cardUrl);

        if ($('#board').val() && $('#list').val()) {
            saveSelectedBoardAndList(
                $('#board').val(),
                $('#list').val(),
                $('#board option:selected').html(),
                $('#list option:selected').html()
            );
        }

        saveTaskData(jiraIssueId, {'shortUrl': cardUrl});
    };
}

function addCardFailure() {
    clearCredentials();
    $('#add-card').hide();
    $('#simple-add').hide();
    $('#msg-failed').show();
}

function getCurrentJiraIssue(callback) {
    chrome.tabs.query({currentWindow: true, active: true}, function(tabs) {
        let url = new URL(tabs[0].url);
        let issueID = /\w*-\d*/.exec(url.pathname)[0];
        callback({url: url, issueID: issueID});
    });
}

function importJiraIssue(button, boardId, listId) {
    button.addClass('disabled');
    button.text('Just a second..');

    getCurrentJiraIssue(function(issueData) {
        let url = issueData.url;
        let issueID = issueData.issueID;
        let endpoint = url.origin + '/rest/api/2/issue/' + issueID;
        $.getJSON(endpoint, function(data) {
            url = url.origin + url.pathname;
            let cardName = data.key + ' ' + data.fields.summary;
            addCard(
                listId, cardName, url, addCardSuccess(issueID), addCardFailure);
        });
    });
}

function handleBoardSelection(event, ui) {
    let boardId = ui.item.value;
    getLists(boardId, function(lists) {
        $('#list').find('option').remove();
        let enableListSelection = false;
        $.each(lists, function(i, list) {
            $('#list').append($('<option></option>')
                       .attr('value', list.id)
                       .text(list.name));
            enableListSelection = true;
        });
        $('#list').selectmenu('refresh')
            .selectmenu((enableListSelection) ? 'enable' : 'disable');
    });
}

function handleAddCardButton() {
    let boardId = $('#board').val();
    let listdId = $('#list').val();
    if (!boardId) {
        highlightLabel($('label[for=\'board-button\']'));
        return;
    }
    if (!listdId) {
        highlightLabel($('label[for=\'list-button\']'));
        return;
    }
    importJiraIssue($(this), boardId, listdId);
}

function displayForm() {
    getBoards(function(boards) {
        $.each(boards, function(i, board) {
            $('#board').append($('<option></option>')
                               .attr('value', board.id)
                               .text(board.name));
        });
        $('#board').selectmenu('refresh');
    });

    $('#board').selectmenu({select: handleBoardSelection});
    $('#btn-addcard').click(handleAddCardButton);

    $('#board').selectmenu()
        .selectmenu({width: 290})
        .selectmenu('menuWidget')
        .addClass('overflow');
    $('#list').selectmenu()
        .selectmenu('disable')
        .selectmenu({width: 290})
        .selectmenu('menuWidget')
        .addClass('overflow');

    $('#simple-add').hide();
    $('#add-card').show();
}

function displayLinkToAddedCard(cardUrl) {
    $('#simple-add').hide();
    $('#add-card').hide();
    $('.card-link').attr('href', cardUrl).text(cardUrl);
    $('#card-added').show();
}

function displaySimpleForm(boardId, listId) {
    let boardName = getSelectedBoardName();
    let listName = getSelectedListName();

    $('#simple-btn-addcard').click(function() {
        $('#simple-btn-change').addClass('disabled');
        importJiraIssue($(this), boardId, listId);
    });
    $('#simple-btn-change').click(function() {
        displayForm();
    });

    $('#board-name').text(boardName);
    $('#list-name').text(listName);
}

function loadPopupContent() {
    getCurrentJiraIssue(function(issueData) {
        let jiraIssueId = issueData.issueID;

        getTaskData(jiraIssueId, function(taskData) {
            if (taskData) {
                displayLinkToAddedCard(taskData.shortUrl);
            } else {
                let boardId = getSelectedBoardId();
                let listId = getSelectedListId();
                if (!(boardId && listId)) {
                    displayForm();
                } else {
                    displaySimpleForm(boardId, listId);
                }
            }
        });
    });
}

$(function() {
    authenticateAccount(false);
    loadPopupContent();
});
