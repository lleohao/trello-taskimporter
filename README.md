[![pipeline status](https://gitlab.com/marspe/trello-taskimporter/badges/master/pipeline.svg)](https://gitlab.com/marspe/trello-taskimporter/commits/master)

Trello TaskImporter
===================

### How to run in development mode?
1. Go to [chrome://extensions](chrome://extensions).
2. Click "Developer mode" in the top right hand corner.
3. Click "Load Unpacked Extension".
4. Choose the `src` folder.
5. Done.

### Install the latest version
[https://chrome.google.com/webstore/detail/import-from-jira-to-trell/codgblfdlbdeklmngicppnomnfegilaj](https://chrome.google.com/webstore/detail/import-from-jira-to-trell/codgblfdlbdeklmngicppnomnfegilaj)